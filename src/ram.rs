use crate::word::Word;
use std::collections::HashMap;

const ADDR_LOWER: Word = 0x0000;
const ADDR_UPPER: Word = 0x6000;

pub struct RAM {
    state: HashMap<Word, Word>,
}

impl RAM {
    /// Creates new instance of RAM.
    pub fn new() -> RAM {
        RAM {
            state: HashMap::new(),
        }
    }

    pub fn read_address(&self, addr: Word) -> Word {
        match self.state.get(&addr) {
            Some(x) => x.to_owned(),
            None => 0x000,
        }
    }

    pub fn write_to_address(&mut self, addr: Word, data: Word) {
        if addr < ADDR_LOWER || addr > ADDR_UPPER {
            return;
        }

        self.state.insert(addr, data);
    }
}

#[cfg(test)]
mod tests {
    use crate::ram::RAM;

    #[test]
    fn reads_and_writes_memory() {
        let ram = RAM::new();

        assert_eq!(ram.read_address(0x0000), 0x0000);
    }

    #[test]
    fn writes_memory() {
        let mut ram = RAM::new();

        ram.write_to_address(0x0000, 0x1337);
        assert_eq!(ram.read_address(0x0000), 0x1337);

        ram.write_to_address(0x7000, 0x1111);
        assert_eq!(ram.read_address(0x7000), 0x0000)
    }
}
