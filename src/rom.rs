use crate::word::Word;

const ADDR_LOWER: Word = 0x0000;
const ADDR_UPPER: Word = 0x6000;

pub struct ROM {
    state: Vec<Word>,
}

impl ROM {
    /// Creates new instance of ROM.
    pub fn new(s: Vec<Word>) -> ROM {
        ROM { state: s }
    }

    pub fn read(&self, i: Word) -> Word {
        if i < ADDR_LOWER || i > ADDR_UPPER {
            return 0;
        }

        self.state[i as usize]
    }
}

#[cfg(test)]
mod tests {
    use crate::rom::ROM;

    #[test]
    fn reads_rom() {
        let program = vec![0x0000, 0xfafa];
        let rom = ROM::new(program);

        assert_eq!(rom.read(0), 0x0000);
        assert_eq!(rom.read(1), 0xfafa);
    }
}
