// A-Instruction
// Used to set the A register to a 15-bit value
// Word signature: 0vvv_vvvv_vvvv_vvvv
// ...where each v is 0 or 1

// C-Instruction
// Used to decide what computation to perform
// Word signature: 111a_cccc_ccdd_djjj
// ...where a - c is the instruction, d is the destination, j is the jump

use crate::ram::RAM;
use crate::rom::ROM;
use crate::word::{negate, not, Word};

#[derive(Debug, PartialEq)]
enum InstructionType {
    A,
    C,
}

#[derive(Debug, PartialEq)]
enum Instruction {
    Zero,
    One,
    NegOne,
    Data,
    Addr,
    NotData,
    NotAddr,
    NegData,
    NegAddr,
    IncData,
    IncAddr,
    DecData,
    DecAddr,
    AddDataAddr,
    SubDataAddr,
    SubAddrData,
    AndDataAddr,
    OrDataAddr,

    Mem,
    NotMem,
    NegMem,
    IncMem,
    DecMem,
    AddDataMem,
    SubDataMem,
    SubMemData,
    AndDataMem,
    OrDataMem,
}

pub struct CPU {
    rom: ROM,
    ram: RAM,
    reg_data: Word,
    reg_addr: Word,
    pc: Word,
}

impl CPU {
    /// Creates new instance of CPU.
    pub fn new(ram: RAM, rom: ROM) -> CPU {
        CPU {
            ram,
            rom,
            reg_addr: 0x0000,
            reg_data: 0x0000,
            pc: 0x0000,
        }
    }

    pub fn get_address_register(&self) -> Word {
        self.reg_addr
    }

    pub fn get_data_register(&self) -> Word {
        self.reg_data
    }

    pub fn get_program_counter(&self) -> Word {
        self.pc
    }

    // pub fn reset(&self) {}

    /// Process next instruction from ROM.
    pub fn step(&mut self) {
        let inst = self.rom.read(self.pc);
        self.execute(inst);
        self.pc += 1;
    }

    fn execute(&mut self, i: Word) {
        match CPU::parse_instruction_type(i) {
            InstructionType::A => self.reg_addr = i & 0b0111_1111_1111_1111,
            InstructionType::C => {
                let instruct = CPU::parse_instruction(i);
                let (set_a, set_d, set_mem) = CPU::instruction_d_value(i);

                let result = match instruct {
                    // a = 0 instructions
                    Instruction::Zero => 0,
                    Instruction::One => 1,
                    Instruction::NegOne => 0b1000_0000_0000_0001,
                    Instruction::Data => self.reg_data,
                    Instruction::Addr => self.reg_addr,
                    Instruction::NotData => not(self.reg_data),
                    Instruction::NotAddr => not(self.reg_addr),
                    Instruction::NegData => negate(self.reg_data),
                    Instruction::NegAddr => negate(self.reg_addr),
                    Instruction::IncData => self.reg_data + 1,
                    Instruction::IncAddr => self.reg_addr + 1,
                    Instruction::DecData => self.reg_data - 1,
                    Instruction::DecAddr => self.reg_addr - 1,
                    Instruction::AddDataAddr => self.reg_data + self.reg_addr,
                    Instruction::SubDataAddr => self.reg_data - self.reg_addr,
                    Instruction::SubAddrData => self.reg_addr - self.reg_data,
                    Instruction::AndDataAddr => self.reg_data & self.reg_addr,
                    Instruction::OrDataAddr => self.reg_data | self.reg_addr,

                    // a = 1 instructions
                    Instruction::Mem => self.read_memory(self.reg_addr),
                    Instruction::NotMem => not(self.read_memory(self.reg_addr)),
                    Instruction::NegMem => negate(self.read_memory(self.reg_addr)),
                    Instruction::IncMem => self.read_memory(self.reg_addr) + 1,
                    Instruction::DecMem => self.read_memory(self.reg_addr) - 1,
                    Instruction::AddDataMem => self.reg_data + self.read_memory(self.reg_addr),
                    Instruction::SubDataMem => self.reg_data - self.read_memory(self.reg_addr),
                    Instruction::SubMemData => self.read_memory(self.reg_addr) - self.reg_data,
                    Instruction::AndDataMem => self.reg_data & self.read_memory(self.reg_addr),
                    Instruction::OrDataMem => self.reg_data | self.read_memory(self.reg_addr),
                };

                if set_a {
                    self.reg_addr = result;
                }

                if set_d {
                    self.reg_data = result;
                }

                if set_mem {
                    self.write_memory(self.reg_addr, result);
                }
            }
        }
    }

    pub fn read_memory(&self, k: Word) -> Word {
        self.ram.read_address(k)
    }

    fn write_memory(&mut self, k: Word, v: Word) {
        self.ram.write_to_address(k, v);
    }

    fn parse_instruction(i: Word) -> Instruction {
        let a = CPU::instruction_a_value(i);
        let c = CPU::instruction_c_value(i);

        if !a {
            match c {
                0b101010 => Instruction::Zero,
                0b111111 => Instruction::One,
                0b111010 => Instruction::NegOne,
                0b001100 => Instruction::Data,
                0b110000 => Instruction::Addr,
                0b001101 => Instruction::NotData,
                0b110001 => Instruction::NotAddr,
                0b001111 => Instruction::NegData,
                0b110011 => Instruction::NegAddr,
                0b011111 => Instruction::IncData,
                0b110111 => Instruction::IncAddr,
                0b001110 => Instruction::DecData,
                0b110010 => Instruction::DecAddr,
                0b000010 => Instruction::AddDataAddr,
                0b010011 => Instruction::SubDataAddr,
                0b000111 => Instruction::SubAddrData,
                0b000000 => Instruction::AndDataAddr,
                0b010101 => Instruction::OrDataAddr,
                _ => Instruction::Zero,
            }
        } else {
            match c {
                0b110000 => Instruction::Mem,
                0b110001 => Instruction::NotMem,
                0b110011 => Instruction::NegMem,
                0b110111 => Instruction::IncMem,
                0b110010 => Instruction::DecMem,
                0b000010 => Instruction::AddDataMem,
                0b010011 => Instruction::SubDataMem,
                0b000111 => Instruction::SubMemData,
                0b000000 => Instruction::AndDataMem,
                0b010101 => Instruction::OrDataMem,
                _ => Instruction::Zero,
            }
        }
    }

    fn parse_instruction_type(i: Word) -> InstructionType {
        match i >> 15 {
            0 => InstructionType::A,
            _ => InstructionType::C,
        }
    }

    fn instruction_a_value(i: Word) -> bool {
        ((0b0001_0000_0000_0000 & i) >> 12) == 1
    }

    fn instruction_c_value(i: Word) -> Word {
        (0b0000_1111_1100_0000 & i) >> 6
    }

    fn instruction_d_value(i: Word) -> (bool, bool, bool) {
        let d_bits = (0b000_0_000000_111_000 & i) >> 3;

        (
            (0b100 & d_bits) >> 2 == 1,
            (0b010 & d_bits) >> 1 == 1,
            (0b001 & d_bits) == 1,
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::cpu::{Instruction, InstructionType, CPU};
    use crate::ram::RAM;
    use crate::rom::ROM;

    #[test]
    fn read_data_register() {
        let ram = RAM::new();
        let rom = ROM::new(vec![]);
        let cpu = CPU::new(ram, rom);

        assert_eq!(cpu.get_data_register(), 0x0000)
    }

    #[test]
    fn read_address_register() {
        let ram = RAM::new();
        let rom = ROM::new(vec![]);
        let cpu = CPU::new(ram, rom);

        assert_eq!(cpu.get_address_register(), 0x0000)
    }

    #[test]
    fn read_program_counter() {
        let ram = RAM::new();
        let rom = ROM::new(vec![]);
        let cpu = CPU::new(ram, rom);

        assert_eq!(cpu.get_program_counter(), 0x0000)
    }

    #[test]
    fn read_write_memory() {
        let ram = RAM::new();
        let rom = ROM::new(vec![]);
        let mut cpu = CPU::new(ram, rom);

        cpu.write_memory(0x00aa, 0x0fff);
        assert_eq!(cpu.read_memory(0x00aa), 0x0fff);
    }

    #[test]
    fn parse_instruction_type() {
        assert_eq!(
            InstructionType::A,
            CPU::parse_instruction_type(0b0_111_1111_1111),
        );

        assert_eq!(
            InstructionType::C,
            CPU::parse_instruction_type(0b111_0_100100_000_000),
        );
    }

    #[test]
    fn parse_instruction_c_value() {
        let block = 0b111_0_100100_000_000;

        assert_eq!(0b100100, CPU::instruction_c_value(block));
    }

    #[test]
    fn parse_instruction_d_value() {
        let block = 0b111_0_000000_100_000;
        assert_eq!((true, false, false), CPU::instruction_d_value(block));

        let block = 0b111_0_000000_111_000;
        assert_eq!((true, true, true), CPU::instruction_d_value(block));
    }

    #[test]
    fn parse_instruction() {
        let block = 0b111_0_111111_000_000;
        assert_eq!(Instruction::One, CPU::parse_instruction(block));

        let block = 0b111_0_110011_000_000;
        assert_eq!(Instruction::NegAddr, CPU::parse_instruction(block));
    }
}
