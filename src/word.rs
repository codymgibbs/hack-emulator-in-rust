/// Memory unit for Hack platform.
pub type Word = u16;

/// Calculates negative value of Word.
///
/// Uses 2's complement calculation.
///
/// # Example
/// ```
/// let negative = negate(0x0001); // 1
/// // negative = 0xffff           // -1
/// ```
pub fn negate(w: Word) -> Word {
    increment(not(w))
}

/// Flips all bits in Word.
pub fn not(w: Word) -> Word {
    !w
}

/// Increments Word by one bit.
pub fn increment(w: Word) -> Word {
    w + 1
}

#[cfg(test)]
mod tests {
    use crate::word::{increment, negate, not, Word};

    #[test]
    fn negation() {
        let w: Word = 0x0001;
        let neg_w: Word = 0xffff;
        assert_eq!(
            negate(w),
            neg_w,
            "negated {:#06x} should be {:#06x}",
            w,
            neg_w
        );
    }

    #[test]
    fn incrementation() {
        let w: Word = 0x0001;
        let inc_w: Word = 0x0002;
        assert_eq!(
            increment(w),
            inc_w,
            "incremented {:#06x} should be {:#06x}",
            w,
            inc_w
        );
    }

    #[test]
    fn inversion() {
        let w: Word = 0x0001;
        let not_w: Word = 0xfffe;
        assert_eq!(
            not(w),
            not_w,
            "inverted {:#06x} should be {:#06x}",
            w,
            not_w
        );
    }
}
