use crate::word::Word;

mod cpu;
mod ram;
mod rom;
mod word;

use crate::cpu::CPU;
use crate::ram::RAM;
use crate::rom::ROM;

/// Assembles new CPU instances with builder pattern.
struct ComputerBuilder {
    program: Option<Vec<Word>>,
}

impl ComputerBuilder {
    /// Creates new instance of ComputerBuilder.
    pub fn new() -> ComputerBuilder {
        ComputerBuilder { program: None }
    }

    /// Modifies program ROM binding manifest.
    ///
    /// # Examples
    /// ```
    /// let mut manifest = ComputerBuilder::new();
    ///
    /// let program = vec![
    ///     // set address_register to 0x00ff
    ///     0b0_000_0000_1111_1111,
    /// ];
    /// manifest.with_program(program);
    /// ```
    pub fn with_program(mut self, program: Vec<Word>) -> ComputerBuilder {
        self.program = Some(program);
        self
    }

    /// Creates new instance of CPU from builder manifest.
    pub fn build(&self) -> CPU {
        let program = match &self.program {
            Some(x) => x.to_owned(),
            None => vec![],
        };

        CPU::new(RAM::new(), ROM::new(program))
    }
}

#[cfg(test)]
mod tests {
    use crate::{ComputerBuilder, Word, CPU};

    /// Grabs a snapshot tuple of CPU registers and RAM value at `mem_addr`.
    ///
    /// # Tuple Structure
    /// ( program counter, address register, data_register, memory value )
    fn snapshot(cpu: &CPU, mem_addr: Word) -> (Word, Word, Word, Word) {
        (
            cpu.get_program_counter(),
            cpu.get_address_register(),
            cpu.get_data_register(),
            cpu.read_memory(mem_addr),
        )
    }

    #[test]
    fn run_program_1() {
        let program = vec![
            // set address_register to 0x00ff
            0b0_000_0000_1111_1111,
        ];

        let mut computer = ComputerBuilder::new().with_program(program).build();

        // computer.reset();
        computer.step();

        assert_eq!(computer.get_address_register(), 0x00ff)
    }

    #[test]
    fn run_program_2() {
        let program = vec![
            // set address_register to 1
            0b111_0_111111_100_000,
        ];

        let mut computer = ComputerBuilder::new().with_program(program).build();

        computer.step();

        assert_eq!(computer.get_address_register(), 0x0001)
    }

    #[test]
    fn run_program_3() {
        let program = vec![
            // addr_register          <= 4
            0b0_000_0000_0100,
            // data_register          <= addr_register + 1
            0b111_0_110111_010_000,
            // memory @ addr_register <= data_register
            0b111_0_001100_001_000,
            // addr_register          <= memory @ addr_register + 1
            0b111_1_110111_100_000,
        ];

        let mut computer = ComputerBuilder::new().with_program(program).build();

        computer.step();
        assert_eq!(snapshot(&computer, 0b100), (1, 0b100, 0b000, 0b000));

        computer.step();
        assert_eq!(snapshot(&computer, 0b100), (2, 0b100, 0b101, 0b000));

        computer.step();
        assert_eq!(snapshot(&computer, 0b100), (3, 0b100, 0b101, 0b101));

        computer.step();
        assert_eq!(snapshot(&computer, 0b100), (4, 0b110, 0b101, 0b101));
    }
}
